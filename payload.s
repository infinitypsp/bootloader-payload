#Copyright (C) 2015, David "Davee" Morgan 

#Permission is hereby granted, free of charge, to any person obtaining a 
#copy of this software and associated documentation files (the "Software"), 
#to deal in the Software without restriction, including without limitation 
#the rights to use, copy, modify, merge, publish, distribute, sublicense, 
#and/or sell copies of the Software, and to permit persons to whom the 
#Software is furnished to do so, subject to the following conditions: 

#The above copyright notice and this permission notice shall be included in 
#all copies or substantial portions of the Software. 

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
#THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
#FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
#DEALINGS IN THE SOFTWARE. 

.set noat
.set noreorder

# Declare stack
addiu $sp, $sp, -16
sw $ra, 0($sp)
sw $s0, 4($sp)
sw $s1, 8($sp)

# Payload addr
move $s0, $s3

# Loadcore text_addr (6.31)
addiu $s1, $ra, -0x1CC4

# Calcuate a call to memlmd_decrypt_patched
la $a0, memlmd_decrypt_patched
addu $a0, $s0, $a0
srl $a0, $a0, 2
li $a1, 0x3ffffff
and $a0, $a0, $a1
li $a1, 0xc000000
or $a0, $a0, $a1

# Patch it (6.31)
sw $a0, 0x6930($s1)

# Fill the stub (#6.31)
lui $v0, 0x0
ori $v0, $v0, 0x8398
addu $v0, $s1, $v0
la $a0, memlmd_decrypt
addu $a0, $s0, $a0
sw $v0, 0xC($a0)

# Force arguments through module_start
# move $a0, $s2
lui $v0, 0x0240
ori $v0, $v0, 0x2021

# 6.31
sw $v0, 0x1CB8($s1)

# addiu $a1, $fp, 196
lui $v0, 0x27C5
ori $v0, $v0, 0x00C4
# 6.31
sw $v0, 0x1CC0($s1)

# Clear the caches via loadcore stubs
lui $v0, 0x0
ori $v0, $v0, 0x8348 #6.31
addu $v0, $s1, $v0
jalr $v0
nop

lui $v0, 0x0
ori $v0, $v0, 0x8350 #6.31
addu $v0, $s1, $v0
jalr $v0
nop

# Return to loadcore
lw $ra, 0($sp)
lw $s0, 4($sp)
lw $s1, 8($sp)
move $v0, $0
jr $ra
addiu $sp, $sp, 16

# Patched function
memlmd_decrypt_patched:
 lw $v1, 0xC($a0)
 li $v0, 0x6E6F6272
 bne $v1, $v0, original
 nop

 lw $v1, 0x4C($a0)
 b read_byte
 addu $a1, $a0, $v1

store_byte:
 sb $v0, 0($a0)
 addiu $a0, $a0, 1

read_byte:
 bnel $a0, $a1, store_byte
 lbu $v0, 0x150($a0)

 sw $v1, 0($a2)
 jr $ra
 move $v0, $0

original:

 move $v0, $ra
 bal memlmd_decrypt
 nop
 lw $ra, 0($ra)
 jr $ra
 move $ra, $v0

# Original function
memlmd_decrypt:
 move $v1, $ra
 jalr $v1
 nop
 .word 0x00000000
