PSPSDK=$(shell psp-config --pspsdk-path)
PSPDEV=$(shell psp-config --pspdev-path)
INCLUDE=$(PSPSDK)/include

all:    payload

clean:
	rm -rf *~ *.o *.elf *.bin

CC       = psp-gcc
CFLAGS   := -D PSP -I $(INCLUDE) -W -Wall -O2 -G0 -fno-pic -mno-abicalls -w -fomit-frame-pointer

ASM      = psp-as

payload.o: payload.s
	$(ASM) payload.s -o payload.o

payload: payload.o linker.l
	$(PSPDEV)/bin/psp-ld -T linker.l -L$(PSPSDK)/lib payload.o -o payload.elf
	$(PSPDEV)/bin/psp-strip -s payload.elf
	$(PSPDEV)/bin/psp-objcopy -O binary payload.elf bootloader_payload.bin
